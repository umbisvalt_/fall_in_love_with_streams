const Stream = require('stream');

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min; // [min, max)
}

class RandomNumberGeneratorStream extends Stream.Readable {
  constructor(numToGenerate, minNum, maxNum, minInterval, maxInterval) {
    super({ objectMode: true });
    this.numToGenerate = numToGenerate;
    this.minNum = minNum;
    this.maxNum = maxNum;
    this.minInterval = minInterval;
    this.maxInterval = maxInterval;
    this.numGenerated = 0;
  }

  _read() {
    console.log('chiamato read');
    this._loop();
  }

  _loop() {
    console.log(`numero: ${this.numGenerated}`);

    const interval = getRandomInt(this.minInterval, this.maxInterval + 1);
    setTimeout(() => {
      if (this.numGenerated >= this.numToGenerate) {
        console.log(`generated ${this.numGenerated} numbers.`);
        return;
      }
      const num = getRandomInt(this.minNum, this.maxNum + 1);
      console.log(`numero generato: ${num}`);
      this.numGenerated += 1;
      if (!this.push(num)) {
        this.once('drain', () => { this._loop(); });
      }
    }, interval);
  }
}

module.exports = {
  RandomNumberGeneratorStream,
};
