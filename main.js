const Stream = require('stream');
const { RandomNumberGeneratorStream } = require('./randomNumberGeneratorStream');

const NUM_TO_GENERATE = 5;
const MIN_NUM = 0;
const MAX_NUM = 500;
const MIN_INTERVAL = 10; // in ms
const MAX_INTERVAL = 100; // in ms

function streamTransform() {
  return new Stream.Transform({
    objectMode: true,
    transform: (data, _, done) => {
      const res = data * 2;
      done(null, res);
    },
  });
}

function renderer() {
  return new Stream.Writable(
    {
      objectMode: true,
      write: (data, _, done) => {
        console.log(data);
        done();
      },
    },
  );
}

const reader = new RandomNumberGeneratorStream(NUM_TO_GENERATE, MIN_NUM, MAX_NUM, MIN_INTERVAL, MAX_INTERVAL);
reader.pipe(streamTransform()).pipe(renderer());
